from flask import Flask, jsonify, Response, request, g
from flask_cors import CORS
from api import api

app = Flask(__name__, static_folder="./dist")
app.register_blueprint(api)


CORS(app, ressources={
     r'/api/*': {'origins': '*'}})


@app.route('/favicon.ico')
def favicon():
    return app.send_static_file("favicon.ico")


@app.route('/js/<path:path>')
def js_files(path):
    return app.send_static_file(f"js/{path}")


@app.route('/css/<path:path>')
def css_files(path):
    return app.send_static_file(f"css/{path}")

@app.route('/', defaults={'path': ''})
@app.route('/<path:path>')
def index(path):
    return app.send_static_file("index.html")


if __name__ == '__main__':
    app.run()
