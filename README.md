# KPI Intelligence - Full-stack Developer Test 🛠

Hi! We're glad you want to join KPI Intelligence! We've designed this test so that _you_ can show us how you think about a problem and how you implement its solution. We'll discuss about it during our next meeting.

## Project

Paris Region wants to have a web application to track the investments it makes for its high schools buildings. They provided you the [`dataset.json`](dataset.json) files containing the existing investments.

### Stage 1: REST API

Write a REST API to retrieve the investments data:

- List all the investments
- List investments filtered by `ville` and/or by `etat_d_avancement`
- Get a single investment by id

Pay attention to the HTTP methods and status codes you use.

### Stage 2: Web application

Implement a simple web application to show the investments:

- Display the list of investments in the form of a table
- Add a form so that we can trigger the API filters
- Add a page to show the details of a single investment

### Bonus stages

> Those are not required but can be cool to do!

- Add an endpoint to edit an investment
- Deploy your application to Heroku
- Display some graphs or interesting figures in the web application
- Have some cool idea? Go on 👍

## Modalities

- You are free to use the languages and tools you are more comfortable with
- Create a repository on GitHub to store your source code and send us the link by e-mail once you've finished
- Please provide a `README` with instructions to run your project

## License

This project is released under the MIT License.

The dataset was released under Open Licence by Région Île-de-France: <https://www.data.gouv.fr/fr/datasets/operations-de-construction-et-de-renovation-dans-les-lycees-francilens/>

## Projet architecture

This project use [posgresql](https://www.postgresql.org/) as database, [flask](https://flask.palletsprojects.com/en/1.1.x/) as back-end framework and [vuejs](https://vuejs.org/) as front-end framework.

An online version is available on [heroku](https://kpi-investment.herokuapp.com/).

### Set up database

You first need to install postgresql cli.

Assuming it's done you first need to `CREATE USER <user_name> WITH PASSWORD <password>` and `CREATE DATABASE <database_name> WITH OWNER <user_name>`

When it's done you have to update the `.env` file values `DB_USER`, `DB_PWD`, `DB_DATABASE` with data used on previous command.

### Set up back-end

Because it's a python framework we recommend using `venv`.

When your venv is set up you can run `pip install -r requirements.txt`.

Be carefull by using `psycopg2` package and not `psycopg2-binary` you might need to also install `libpq` with your os package manager (ex: `apt install libpq`).

You can fill your database using `python setup_database.py`, that will create investments TABLE and populate it with data from [`dataset.json`](dataset.json)

Then to start back-end please do:

```bash
source .env
export FLASK_APP=app.py
flask run
```

Back-end will listen to port 5000.

### Set up front-end

You will need `npm` to perfom commands.

First you have to install dependencies `npm i`

Then you can run `npm run serve`. Front-end will listen to port 8080.
