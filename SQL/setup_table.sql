DROP TABLE IF EXISTS investments CASCADE;

CREATE TABLE investments (
    id                              SERIAL NOT NULL,
    titreoperation                  VARCHAR(300) NOT NULL,
    ville                           VARCHAR(40) NOT NULL,
    ppi                             CHAR(9) NOT NULL,
    lycee                           VARCHAR(50) NOT NULL,
    codeuai                         CHAR(8) NOT NULL,
    etat_d_avancement               VARCHAR(40) NOT NULL,
    montant_des_ap_votes_en_meu     DOUBLE PRECISION,
    annee_d_individualisation       INTEGER,
    longitude                       DOUBLE PRECISION,
    latitude                        DOUBLE PRECISION,
    enveloppe_prev_en_meu           DOUBLE PRECISION,
    maitrise_d_oeuvre               VARCHAR(200),
    mandataire                      VARCHAR(200),
    entreprise                      VARCHAR(200),
    notification_du_marche          DATE,
    annee_de_livraison              INTEGER,
    cao_attribution                 DATE,
    mode_de_devolution              VARCHAR(50),
    nombre_de_lots                  INTEGER,
    CONSTRAINT pk_investments PRIMARY KEY (id)
);