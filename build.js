const { exec } = require("child_process");

const env = process.env.NODE_ENV;

var cmd = undefined;

if (env == "production") {
  cmd = "vue-cli-service build";
} else {
  cmd = "vue-cli-service build --mode development";
}

exec(cmd, function (error, stdout, stderr) {
  console.log("stdout: " + stdout);
  console.log("stderr: " + stderr);
  if (error !== null) {
    console.log("exec error: " + error);
  }
});
