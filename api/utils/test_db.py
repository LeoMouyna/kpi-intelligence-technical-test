import pytest
from api.utils.db import set_query_conditions, set_query_updates, DatabaseException

args_params = [
    {"name": "ville", "operator": "like"},
    {"name": "ppi", "operator": "eq"},
    {"name": "codeuai"}
]


def test_set_query_conditions():
    empty_request_params = {}
    base_query = "SELECT * FROM test"

    query, params = set_query_conditions(
        args_params=args_params, request_args=empty_request_params, base_query=base_query)

    assert query == base_query
    assert len(params.keys()) == 0

    same_request_params = {"ville": "pari",
                           "ppi": "2002/2005", "codeuai": "QWERT12345"}
    query, params = set_query_conditions(
        args_params=args_params, request_args=same_request_params, base_query=base_query)

    assert params['ppi'] == same_request_params['ppi']
    assert params['ville'] == "%pari%"
    assert query == "SELECT * FROM test WHERE lower(ville) LIKE %(ville)s AND ppi = %(ppi)s AND codeuai = %(codeuai)s"

    different_request_params = {"toto": "test",
                                "brole": "issu", "codeuai": "QWERT12345"}

    query, params = set_query_conditions(
        args_params=args_params, request_args=different_request_params, base_query=base_query)
    assert params['codeuai'] == different_request_params['codeuai']
    assert params.get('toto') is None
    assert query == "SELECT * FROM test WHERE codeuai = %(codeuai)s"


def test_set_query_updates():
    request_data = {}
    base_query = "UPDATE test"
    condition_query = "WHERE id = %(id)s"

    with pytest.raises(DatabaseException):
        query = set_query_updates(request_data=request_data, base_query=base_query, condition_query=condition_query)
        assert query == "UPDATE test WHERE id = %(id)s"

    request_data = dict(
        test = True,
        population = 1000,
        name = "Stonk"
    )

    query = set_query_updates(request_data=request_data, base_query=base_query, condition_query=condition_query)
    assert query == "UPDATE test SET test = %(test)s, population = %(population)s, name = %(name)s WHERE id = %(id)s"