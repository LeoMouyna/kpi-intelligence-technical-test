import pytest
from api.utils.pagination import paginate_list
from werkzeug.datastructures import ImmutableMultiDict


def test_paginate_list():
    listing = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
    req_args = ImmutableMultiDict([("limit", 10), ("offset", 0)])

    assert paginate_list(listing, req_args) == listing[:10]
    assert paginate_list(listing, ImmutableMultiDict()) == listing

    req_args = ImmutableMultiDict([("limit", 10), ("offset", 20)])
    assert paginate_list(listing, req_args) == []
