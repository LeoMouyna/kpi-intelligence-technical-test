import psycopg2
import psycopg2.extras
from psycopg2.extensions import cursor
from typing import Dict, List
from werkzeug.datastructures import ImmutableMultiDict
import logging
from os.path import join
from api.utils.config import SQL_DIRECTORY


class DatabaseException(Exception):
    def __init__(self, msg: str):
        super().__init__(msg)


class DuplicateException(DatabaseException):
    def __init__(self, msg: str):
        super().__init__(msg)


class Database:
    def __init__(self, user: str = None, pwd: str = None, database: str = None, url: str = None, ssl_mode: str = None):
        session = None
        if user and pwd and database:
            session = psycopg2.connect(
                user=user,
                password=pwd,
                database=database
            )
        elif url and ssl_mode:
            session = psycopg2.connect(url, sslmode=ssl_mode)
        if not session:
            raise DatabaseException("Can't connect to database.")
        self.user = user,
        self.database = database
        self.url = url
        self.ssl_mode = ssl_mode
        self.session = session

    def new_cursor(self):
        """Return a new db cursor
        """
        return self.session.cursor(cursor_factory=psycopg2.extras.DictCursor)

    def perform_query(self, query: str, params: Dict = None, name: str = ""):
        """Execute a query and wrap some common exception

        Args:
            query (str): sql query to execute in database
            params (Dict, optional): parameters added to the query. Defaults to None.
            name (str, optional): query name for log purpose. Defaults to "".

        Raises:
            DuplicateException: raised if element already exist in database during insert.
            DatabaseException: raised if any other error happen.
        """
        dbcur = self.new_cursor()
        try:
            dbcur.execute(query, params)
            return dbcur
        except Exception as ex:
            dbcur.connection.rollback()
            if type(ex).__name__ == "UniqueViolation":
                raise DuplicateException(f"Duplicate data: {ex}")
            dsp_name = name if name else query
            raise DatabaseException(
                f"ERROR: unable to perform {dsp_name} query with params {params} ({type(ex).__name__}: {ex})")

    def perform_query_from_file(self, sql_file: str, query_name: str = None, query_params: Dict = None, commit: bool = False):
        """Perform a query get from a file safely.
            i.e. use savepoint at the begining of transaction and go back to it if failure happens.

        Args:
            sql_file (str): file path containing SQL request.
            query_name (str, optional): query name displayed on logs. Defaults to None.
            query_params (Dict, optional): parameters added to the query. Defaults to None.
            commit (bool, optional): Should commit changes ? Defaults to False.

        Raises:
            ValueError: If file is empty.
            DatabaseException: If database interaction fails.
        """
        if not query_name:
            # Set query name according to sql file name
            query_name = sql_file.split('.')[0].replace('_', ' ')

        logging.info(f'Start {query_name}')

        try:
            with open(sql_file) as sql:
                query = sql.read()
                if len(query) == 0:
                    raise ValueError(f"File: {sql_file} got empty request.")
                dbcur = self.perform_query(
                    query, name=query_name, params=query_params)

            if commit:
                self.session.commit()

            return dbcur
        except DatabaseException as e:
            logging.error(e)
            if commit:
                self.session.rollback()
            raise e


def set_query_conditions(args_params: List, request_args: ImmutableMultiDict, base_query: str):
    conditions = []
    query_params = {}

    for arg in args_params:
        key = arg['name']
        operator = arg.get('operator')
        value = request_args.get(key)
        if value:
            if operator == 'like':
                condition = f"lower({key}) LIKE %({key})s"
                value = f"%{value.lower()}%"
            else:
                condition = f"{key} = %({key})s"

            conditions.append(condition)
            query_params[key] = value

    query = base_query
    if conditions:
        query += " WHERE " + \
            " AND ".join(conditions)

    return (query, query_params)


def set_query_updates(request_data: Dict, base_query: str, condition_query: str):
    set_querys = []

    for key in request_data.keys():
        set_query = f"{key} = %({key})s"
        set_querys.append(set_query)

    if not set_querys:
        raise DatabaseException(f"To update element we need new values and data are {request_data}")
    
    return f"{base_query} SET {', '.join(set_querys)} {condition_query}"
    
