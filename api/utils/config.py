from os import environ, path

SQL_DIRECTORY = path.join(path.dirname(path.abspath(__file__)), "..", "SQL")

DB_USER = environ.get("DB_USER")
DB_PWD = environ.get("DB_PWD")
DB_DATABASE = environ.get("DB_DATABASE")

DB_DATABASE_URL = environ.get("DATABASE_URL")
