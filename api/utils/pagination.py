from typing import List, Dict
from werkzeug.datastructures import ImmutableMultiDict


def paginate_list(list: List, request_args: ImmutableMultiDict):
    limit = request_args.get('limit', default=len(list), type=int)
    offset = request_args.get('offset', default=0, type=int)
    max_index = min(len(list), offset + limit)

    return list[offset:max_index]
