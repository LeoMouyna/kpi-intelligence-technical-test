import pytest
import datetime
from api.models.investment import Investment, InvestmentException, InvestmentEncoder
from json import dumps

not_full_list = [1, 'Ravalement des façades, mise en conformité des ateliers et création foyer des élèves', 'Paris 12ème', '2001/2006', "Métiers-de-l'ameublement", '0750784V', 'Opération livrée',
                 1.0, 1998, 2.39150432700006, 48.846753735, 0.43, 'Laumonnier Menninger / SLI', 'SEMAEST', 'Coulon SA', datetime.date(2002, 10, 28), 2006, datetime.date(2005, 10, 2), 'Ent générale', None]

full_list = [1, 'Ravalement des façades, mise en conformité des ateliers et création foyer des élèves', 'Paris 12ème', '2001/2006', "Métiers-de-l'ameublement", '0750784V', 'Opération livrée',
             1.0, 1998, 2.39150432700006, 48.846753735, 0.43, 'Laumonnier Menninger / SLI', 'SEMAEST', 'Coulon SA', datetime.date(
                 2002, 10, 28), 2006, datetime.date(2005, 10, 2), 'Ent générale', 2]

not_right_len_list = [1, 2, 3]


def test_from_list():
    investment = Investment.from_list(not_full_list)
    assert investment.titreoperation == 'Ravalement des façades, mise en conformité des ateliers et création foyer des élèves'
    assert investment.codeuai == '0750784V'
    assert investment.nombre_de_lots is None

    investment = Investment.from_list(full_list)
    assert investment.nombre_de_lots is 2

    with pytest.raises(InvestmentException):
        investment = Investment.from_list(not_right_len_list)


not_full_dict = {
    "titreoperation": "Ravalement des façades, mise en conformité des ateliers et création foyer des élèves",
    "entreprise": "Coulon SA",
    "annee_de_livraison": "2006",
    "ville": "Paris 12ème",
    "mandataire": "SEMAEST",
    "ppi": "2001/2006",
    "lycee": "Métiers-de-l'ameublement",
    "notification_du_marche": "2002-10-28",
    "codeuai": "0750784V",
    "cao_attribution": "2005-10-02",
    "maitrise_d_oeuvre": "Laumonnier Menninger / SLI",
}

full_dict = {
    "titreoperation": "Ravalement des façades, mise en conformité des ateliers et création foyer des élèves",
    "entreprise": "Coulon SA",
    "annee_de_livraison": "2006",
    "ville": "Paris 12ème",
    "mandataire": "SEMAEST",
    "ppi": "2001/2006",
    "lycee": "Métiers-de-l'ameublement",
    "notification_du_marche": "2002-10-28",
    "codeuai": "0750784V",
    "longitude": 2.391504327000064,
    "etat_d_avancement": "Opération livrée",
    "montant_des_ap_votes_en_meu": 1,
    "cao_attribution": "2005-10-02",
    "latitude": 48.84675373500005,
    "maitrise_d_oeuvre": "Laumonnier Menninger / SLI",
    "mode_de_devolution": "Ent générale",
    "annee_d_individualisation": "1998",
    "enveloppe_prev_en_meu": 0.43,
    "nombre_de_lots": 2
}


def test_from_dict():
    investment = Investment.from_dict(not_full_dict)
    assert investment.titreoperation == 'Ravalement des façades, mise en conformité des ateliers et création foyer des élèves'
    assert investment.codeuai == '0750784V'
    assert investment.nombre_de_lots is None
    assert investment.enveloppe_prev_en_meu is None

    investment = Investment.from_dict(full_dict)
    assert investment.notification_du_marche == datetime.date(2002, 10, 28)
    assert investment.nombre_de_lots is 2


json_str = """{"id": null, "titreoperation": "Ravalement des fa\\u00e7ades, mise en conformit\\u00e9 des ateliers et cr\\u00e9ation foyer des \\u00e9l\\u00e8ves", "ville": "Paris 12\\u00e8me", "ppi": "2001/2006", "lycee": "M\\u00e9tiers-de-l\'ameublement", "codeuai": "0750784V", "etat_d_avancement": "Op\\u00e9ration livr\\u00e9e", "montant_des_ap_votes_en_meu": 1, "annee_d_individualisation": "1998", "longitude": 2.391504327000064, "latitude": 48.84675373500005, "enveloppe_prev_en_meu": 0.43, "maitrise_d_oeuvre": "Laumonnier Menninger / SLI", "mandataire": "SEMAEST", "entreprise": "Coulon SA", "notification_du_marche": "2002-10-28", "annee_de_livraison": "2006", "cao_attribution": "2005-10-02", "mode_de_devolution": "Ent g\\u00e9n\\u00e9rale", "nombre_de_lots": 2}"""


def test_encoder():
    investment = Investment.from_dict(full_dict)
    assert dumps(investment, cls=InvestmentEncoder) == json_str
