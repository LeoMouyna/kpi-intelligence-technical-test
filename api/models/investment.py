from datetime import date, datetime
from typing import List, Dict
from json import JSONEncoder


date_format = '%Y-%m-%d'


class InvestmentException(Exception):
    def __init__(self, msg: str):
        super().__init__(msg)


class Investment:
    def __init__(
        self,
        id: int,
        titreoperation: str,
        ville: str,
        ppi: str,
        lycee: str,
        codeuai: str,
        etat_d_avancement: str,
        montant_des_ap_votes_en_meu: float = None,
        annee_d_individualisation: int = None,
        longitude: float = None,
        latitude: float = None,
        enveloppe_prev_en_meu: float = None,
        maitrise_d_oeuvre: str = None,
        mandataire: str = None,
        entreprise: str = None,
        notification_du_marche: date = None,
        annee_de_livraison: int = None,
        cao_attribution: date = None,
        mode_de_devolution: str = None,
        nombre_de_lots: int = None,
        *args,
        **kwargs
    ):
        self.id = id
        self.titreoperation = titreoperation
        self.ville = ville
        self.ppi = ppi
        self.lycee = lycee
        self.codeuai = codeuai
        self.etat_d_avancement = etat_d_avancement
        self.montant_des_ap_votes_en_meu = montant_des_ap_votes_en_meu
        self.annee_d_individualisation = annee_d_individualisation
        self.longitude = longitude
        self.latitude = latitude
        self.enveloppe_prev_en_meu = enveloppe_prev_en_meu
        self.maitrise_d_oeuvre = maitrise_d_oeuvre
        self.mandataire = mandataire
        self.entreprise = entreprise
        self.notification_du_marche = notification_du_marche
        self.annee_de_livraison = annee_de_livraison
        self.cao_attribution = cao_attribution
        self.mode_de_devolution = mode_de_devolution
        self.nombre_de_lots = nombre_de_lots

    @classmethod
    def from_list(cls, attr_list: List):
        if len(attr_list) != 20:
            raise InvestmentException(
                f"List doesn't contain the exact amount of 20 items: {attr_list}")

        return cls(
            attr_list[0],
            attr_list[1],
            attr_list[2],
            attr_list[3],
            attr_list[4],
            attr_list[5],
            attr_list[6],
            attr_list[7],
            attr_list[8],
            attr_list[9],
            attr_list[10],
            attr_list[11],
            attr_list[12],
            attr_list[13],
            attr_list[14],
            attr_list[15],
            attr_list[16],
            attr_list[17],
            attr_list[18],
            attr_list[19],
        )

    @classmethod
    def from_dict(cls, attr_dict: Dict):
        notification_du_marche = attr_dict.get("notification_du_marche")
        notification_du_marche = datetime.strptime(
            notification_du_marche, date_format).date() if notification_du_marche else None
        cao_attribution = attr_dict.get("cao_attribution")
        cao_attribution = datetime.strptime(
            cao_attribution, date_format).date() if cao_attribution else None
        return cls(
            id=attr_dict.get("id"),
            titreoperation=attr_dict.get("titreoperation"),
            ville=attr_dict.get("ville"),
            ppi=attr_dict.get("ppi"),
            lycee=attr_dict.get("lycee"),
            codeuai=attr_dict.get("codeuai"),
            etat_d_avancement=attr_dict.get("etat_d_avancement"),
            montant_des_ap_votes_en_meu=attr_dict.get(
                "montant_des_ap_votes_en_meu"),
            annee_d_individualisation=attr_dict.get(
                "annee_d_individualisation"),
            longitude=attr_dict.get("longitude"),
            latitude=attr_dict.get("latitude"),
            enveloppe_prev_en_meu=attr_dict.get("enveloppe_prev_en_meu"),
            maitrise_d_oeuvre=attr_dict.get("maitrise_d_oeuvre"),
            mandataire=attr_dict.get("mandataire"),
            entreprise=attr_dict.get("entreprise"),
            notification_du_marche=notification_du_marche,
            annee_de_livraison=attr_dict.get("annee_de_livraison"),
            cao_attribution=cao_attribution,
            mode_de_devolution=attr_dict.get("mode_de_devolution"),
            nombre_de_lots=attr_dict.get("nombre_de_lots")
        )


class InvestmentEncoder(JSONEncoder):
    def default(self, o: Investment):
        if isinstance(o, Investment):
            return dict(
                id=o.id,
                titreoperation=o.titreoperation,
                ville=o.ville,
                ppi=o.ppi,
                lycee=o.lycee,
                codeuai=o.codeuai,
                etat_d_avancement=o.etat_d_avancement,
                montant_des_ap_votes_en_meu=o.montant_des_ap_votes_en_meu,
                annee_d_individualisation=o.annee_d_individualisation,
                longitude=o.longitude,
                latitude=o.latitude,
                enveloppe_prev_en_meu=o.enveloppe_prev_en_meu,
                maitrise_d_oeuvre=o.maitrise_d_oeuvre,
                mandataire=o.mandataire,
                entreprise=o.entreprise,
                notification_du_marche=o.notification_du_marche,
                annee_de_livraison=o.annee_de_livraison,
                cao_attribution=o.cao_attribution,
                mode_de_devolution=o.mode_de_devolution,
                nombre_de_lots=o.nombre_de_lots,
            )
        elif isinstance(o, date):
            return str(o)
        else:
            return super().default(o)
