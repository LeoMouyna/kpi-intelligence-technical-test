from flask import Blueprint, request, jsonify, Response
from api.utils.config import DB_USER, DB_DATABASE, DB_PWD, DB_DATABASE_URL
from api.utils.db import Database, DuplicateException, DatabaseException, set_query_conditions, set_query_updates
from api.utils.pagination import paginate_list
from api.models.investment import Investment as MInvestment, InvestmentEncoder
import ast


api = Blueprint('api', __name__, url_prefix='/api')
api.json_encoder = InvestmentEncoder

if DB_DATABASE_URL:
    db = Database(url=DB_DATABASE_URL, ssl_mode='require')

elif DB_DATABASE and DB_USER and DB_PWD:
    db = Database(user=DB_USER, pwd=DB_PWD, database=DB_DATABASE)


@api.route('/v1/investments', methods=['GET'])
def investments():
    supported_params = [
        {"name": "ville", "operator": "like"},
        {"name": "etat_d_avancement", "operator": "like"},
        {"name": "codeuai"},
        {"name": "ppi"},
    ]

    query = "SELECT * FROM investments"

    query, query_params = set_query_conditions(
        args_params=supported_params,
        request_args=request.args,
        base_query=query
    )

    db_res = db.perform_query(query=query, params=query_params).fetchall()
    pretty_res = []
    for res in db_res:
        pretty_res.append(MInvestment.from_list(res))

    # Pagination
    paginated_list = paginate_list(pretty_res, request.args)

    return jsonify(paginated_list)


@api.route('/v1/investments/<int:id>', methods=['GET', 'PUT'])
def investment(id):
    if request.method == 'GET':
        query = "SELECT * FROM investments WHERE id=%(id)s"
        query_params = dict(id=id)
        db_res = db.perform_query(query=query, params=query_params).fetchone()
        if db_res is None:
            return f"Investment with id {id} not found", 404
        pretty_res = MInvestment.from_list(db_res)
        return jsonify(pretty_res)

    elif request.method == 'PUT':

        data_str = request.data.decode("UTF-8")
        data = ast.literal_eval(data_str)

        base_query = "UPDATE investments"
        condition_query = "WHERE id = %(id)s"
        query = set_query_updates(data, base_query, condition_query)
        data['id'] = id
        db.perform_query(query, params=data, name=f"Update investment #{id}")
        db.session.commit()
        return jsonify(data)
