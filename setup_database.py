from api.utils.db import Database
from api.utils.config import DB_DATABASE, DB_PWD, DB_USER, DB_DATABASE_URL
from os.path import join, dirname, abspath
from json import loads

if DB_DATABASE_URL:
    db = Database(url=DB_DATABASE_URL, ssl_mode='require')

elif DB_USER and DB_PWD and DB_DATABASE:
    db = Database(
        user=DB_USER,
        pwd=DB_PWD,
        database=DB_DATABASE
    )

db.perform_query_from_file(
    sql_file=join(dirname(abspath(__file__)), "SQL", "setup_table.sql"),
    query_name="Setup database"
)

with open('dataset.json') as file:
    investments = loads(file.read())
    field_count = dict()
    for investment in investments:
        columns = ", ".join(investment.keys())
        values = [str(v) for v in investment.values()]
        insert_value = ", ".join(values)
        query = f"""
            INSERT INTO investments (
                titreoperation,
                ville,
                ppi,
                lycee,
                codeuai,
                etat_d_avancement,
                montant_des_ap_votes_en_meu,
                annee_d_individualisation,
                longitude,
                latitude,
                enveloppe_prev_en_meu,
                maitrise_d_oeuvre,
                mandataire,
                entreprise,
                notification_du_marche,
                annee_de_livraison,
                cao_attribution,
                mode_de_devolution,
                nombre_de_lots
            )
            VALUES (
                %(titreoperation)s,
                %(ville)s,
                %(ppi)s,
                %(lycee)s,
                %(codeuai)s,
                %(etat_d_avancement)s,
                %(montant_des_ap_votes_en_meu)s,
                %(annee_d_individualisation)s,
                %(longitude)s,
                %(latitude)s,
                %(enveloppe_prev_en_meu)s,
                %(maitrise_d_oeuvre)s,
                %(mandataire)s,
                %(entreprise)s,
                %(notification_du_marche)s,
                %(annee_de_livraison)s,
                %(cao_attribution)s,
                %(mode_de_devolution)s,
                %(nombre_de_lots)s

            );
        """
        query_params = dict(
            titreoperation=investment.get('titreoperation'),
            ville=investment.get('ville'),
            ppi=investment.get('ppi'),
            lycee=investment.get('lycee'),
            codeuai=investment.get('codeuai'),
            etat_d_avancement=investment.get('etat_d_avancement'),
            montant_des_ap_votes_en_meu=investment.get(
                'montant_des_ap_votes_en_meu'),
            annee_d_individualisation=investment.get(
                'annee_d_individualisation'),
            longitude=investment.get('longitude'),
            latitude=investment.get('latitude'),
            enveloppe_prev_en_meu=investment.get('enveloppe_prev_en_meu'),
            maitrise_d_oeuvre=investment.get('maitrise_d_oeuvre'),
            mandataire=investment.get('mandataire'),
            entreprise=investment.get('entreprise'),
            notification_du_marche=investment.get('notification_du_marche'),
            annee_de_livraison=investment.get('annee_de_livraison'),
            cao_attribution=investment.get('cao_attribution'),
            mode_de_devolution=investment.get('mode_de_devolution'),
            nombre_de_lots=investment.get('nombre_de_lots')

        )
        db.perform_query(
            query=query,
            params=query_params,
            name=f"Populate with {investment['codeuai']} investment"
        )

db.session.commit()
db.session.close()
