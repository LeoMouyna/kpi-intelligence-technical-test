import Vue from "vue";
import VueRouter, { RouteConfig } from "vue-router";
import Investments from "../views/Investments.vue";

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  {
    path: "/",
    name: "Home",
    component: Investments,
  },
  {
    path: "/investment/:id",
    name: "Investment Detail",
    props: (route) => {
      const investmentId = Number.parseInt(route.params.id, 10);
      if (Number.isNaN(investmentId)) {
        return 0;
      }
      return { investmentId };
    },
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/InvestmentDetail.vue"),
  },
  {
    path: "/investment/:id/edit",
    name: "Investment Edition",
    props: (route) => {
      const investmentId = Number.parseInt(route.params.id, 10);
      if (Number.isNaN(investmentId)) {
        return 0;
      }
      return { investmentId };
    },
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/InvestmentEdition.vue"),
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
