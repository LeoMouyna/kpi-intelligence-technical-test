import { InvestmentI } from "@/interfaces/investment";
import store from "@/store";
import {
  VuexModule,
  Module,
  Mutation,
  Action,
  getModule,
} from "vuex-module-decorators";

export interface InvestmentStateI {
  list: Array<InvestmentI>;
  selected: InvestmentI | null;
}

@Module({ dynamic: true, store, namespaced: true, name: "investment" })
class Investment extends VuexModule implements InvestmentStateI {
  public list: InvestmentI[] = [];
  public selected: InvestmentI | null = null;

  @Mutation
  private SET_INVESTMENTS(investments: InvestmentI[]): void {
    this.list = investments;
  }
  @Mutation
  private SET_SELECTED_INVESTMENT(investment: InvestmentI): void {
    this.selected = investment;
  }
  @Mutation
  private CLEAR_SELECTED_INVESTMENT(): void {
    this.selected = null;
  }

  @Action
  public updateInvestments(investments: InvestmentI[]): void {
    this.SET_INVESTMENTS(investments);
  }
  @Action
  public setInvestment(investment: InvestmentI): void {
    this.SET_SELECTED_INVESTMENT(investment);
  }
  @Action
  public clearInvestment(): void {
    this.CLEAR_SELECTED_INVESTMENT();
  }
}

export const InvestmentModule = getModule(Investment);
