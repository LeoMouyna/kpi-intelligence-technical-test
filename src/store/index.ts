import Vue from "vue";
import Vuex from "vuex";
import { InvestmentStateI } from "@/store/modules/investments";

Vue.use(Vuex);

export interface RootStateI {
  investment: InvestmentStateI;
}

export default new Vuex.Store<RootStateI>({});
