import { API } from "@/service/httpService";
import { InvestmentI } from "@/interfaces/investment";

export default class InvestmentRequest {
  getOne(investmentId: number) {
    return API.get(`investments/${investmentId}`);
  }

  getSeveral(params?: any) {
    return API.get("investments", { params: params });
  }

  update(investment: InvestmentI) {
    return API.put(`investments/${investment.id}`, investment);
  }
}
