import axios from "axios";

const urlSlug = process.env.VUE_APP_urlSlug;
const baseUrl = urlSlug
  ? `https://${urlSlug}.herokuapp.com`
  : "http://localhost:5000";

export const API = axios.create({
  baseURL: `${baseUrl}/api/v1`,
  headers: {
    "Access-Control-Allow-Origin": "*",
    "Content-Type": "application/json",
  },
});
