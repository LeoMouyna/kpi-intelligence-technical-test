export interface InvestmentI {
  id: number;
  titreoperation: String;
  ville: String;
  ppi: String;
  lycee: String;
  codeuai: String;
  etat_d_avancement: String;
  montant_des_ap_votes_en_meu?: number;
  annee_d_individualisation?: number;
  longitude?: number;
  latitude?: number;
  enveloppe_prev_en_meu?: number;
  maitrise_d_oeuvre?: String;
  mandataire?: String;
  entreprise?: String;
  notification_du_marche?: Date;
  annee_de_livraison?: number;
  cao_attribution?: Date;
  mode_de_devolution?: String;
  nombre_de_lots?: number;
}
