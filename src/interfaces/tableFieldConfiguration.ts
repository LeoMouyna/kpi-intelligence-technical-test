export interface TableFieldConfigurationI {
  title: String;
  field: String;
  visible: boolean;
  concealable?: boolean;
  helpbox?: String;
  sortable: boolean;
}
